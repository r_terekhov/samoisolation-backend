package pt.samoisolationcore.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pt.samoisolationcore.domain.Feedback;
import pt.samoisolationcore.dto.GiveFeedbackDto;
import pt.samoisolationcore.repository.FeedbackRepository;
import pt.samoisolationcore.service.FeedbackService;

import javax.validation.Valid;

@Service
public class FeedbackServiceImpl implements FeedbackService {
    private static final Logger log = LoggerFactory.getLogger(FeedbackService.class);
    private final FeedbackRepository feedbackRepository;

    public FeedbackServiceImpl(FeedbackRepository feedbackRepository) {
        this.feedbackRepository = feedbackRepository;
    }

    @Override
    public Feedback giveFeedback(@Valid GiveFeedbackDto giveFeedbackDto) {
        return feedbackRepository.save(new Feedback(giveFeedbackDto.getRating(), giveFeedbackDto.getContent()));
    }
}
