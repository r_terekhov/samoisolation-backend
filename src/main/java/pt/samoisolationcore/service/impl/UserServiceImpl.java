package pt.samoisolationcore.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pt.samoisolationcore.domain.User;
import pt.samoisolationcore.dto.UserDto;
import pt.samoisolationcore.dto.UserRegistrationRequestDto;
import pt.samoisolationcore.model.Roles;
import pt.samoisolationcore.repository.UserRepository;
import pt.samoisolationcore.repository.UserRoleRepository;
import pt.samoisolationcore.service.UserService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger log = LoggerFactory.getLogger(UserService.class);
    private final BCryptPasswordEncoder passwordEncoder;
    private final UserRoleRepository userRoleRepository;
    private final UserRepository userRepository;

    public UserServiceImpl(BCryptPasswordEncoder passwordEncoder, UserRoleRepository userRoleRepository, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRoleRepository = userRoleRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Boolean checkIfUserExists(@NotBlank String nick) {
        return userRepository.findUserByNick(nick).isPresent();
    }

    @Override
    public UserDto register(@Valid UserRegistrationRequestDto userRegistrationRequestDto) {
        User newUser = new User(
                userRegistrationRequestDto.getNick(),
                passwordEncoder.encode(userRegistrationRequestDto.getPassword()),
                userRegistrationRequestDto.getAge(),
                userRegistrationRequestDto.getGender(),
                List.of(userRoleRepository.findUserRoleByName(Roles.USER.name()).get())
        );

        return UserDto.of(userRepository.save(newUser));
    }
}
