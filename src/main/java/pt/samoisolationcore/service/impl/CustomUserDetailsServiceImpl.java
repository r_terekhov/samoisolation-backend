/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package pt.samoisolationcore.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pt.samoisolationcore.config.security.CustomUserDetails;
import pt.samoisolationcore.domain.User;
import pt.samoisolationcore.repository.UserRepository;

import java.text.MessageFormat;

@Service
public class CustomUserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public CustomUserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String nick) throws UsernameNotFoundException {
        User user = userRepository.findUserByNick(nick).orElseThrow(() -> new UsernameNotFoundException(MessageFormat.format("User with nick {0} not found", nick)));
        return new CustomUserDetails(user);
    }

}