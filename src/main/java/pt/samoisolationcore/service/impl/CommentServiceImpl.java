package pt.samoisolationcore.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pt.samoisolationcore.config.security.CustomUserDetails;
import pt.samoisolationcore.domain.Comment;
import pt.samoisolationcore.domain.User;
import pt.samoisolationcore.dto.CommentDto;
import pt.samoisolationcore.dto.CreateCommentRequestDto;
import pt.samoisolationcore.repository.CommentRepository;
import pt.samoisolationcore.repository.HubRepository;
import pt.samoisolationcore.repository.UserRepository;
import pt.samoisolationcore.service.CommentService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

import static java.text.MessageFormat.format;

@Service
public class CommentServiceImpl implements CommentService {
    private static final Logger log = LoggerFactory.getLogger(CommentService.class);
    private final CommentRepository commentRepository;
    private final HubRepository hubRepository;
    private final UserRepository userRepository;

    public CommentServiceImpl(CommentRepository commentRepository, HubRepository hubRepository, UserRepository userRepository) {
        this.commentRepository = commentRepository;
        this.hubRepository = hubRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<CommentDto> getComments(@NotBlank String hubId, @NotNull Integer page, @NotNull Integer commentsPageSize) {
        Page<Comment> commentPage = commentRepository.findAllByHubIdOrderByCreatedAsc(hubId, PageRequest.of(page, CommentService.PAGE_SIZE));
        int totalPages = commentPage.getTotalPages();
        if (page >= totalPages) {
            return null;
        }

        return commentPage.get()
                .map(CommentDto::of)
                .collect(Collectors.toList());

    }

    @Override
    public CommentDto createComment(@Valid CreateCommentRequestDto createCommentRequestDto) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) {
            throw new AuthorizationServiceException("need to authorize");
        }

        CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
        User user = userRepository.findById(userDetails.getUserId()).orElseThrow(() -> new RuntimeException(format("user not found by id {0} on creating comment", userDetails.getUserId())));

        Comment newComment = new Comment(
                userDetails.getUserId(),
                user.getNick(),
                user.getAge(),
                user.getGender(),
                createCommentRequestDto.getHubId(),
                createCommentRequestDto.getContent()
        );

        //todo make it in transaction
        Comment savedComment = commentRepository.save(newComment);
        hubRepository.incCommentsCounter(createCommentRequestDto.getHubId());

        return CommentDto.of(savedComment);
    }
}
