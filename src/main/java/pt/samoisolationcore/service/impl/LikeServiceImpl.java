package pt.samoisolationcore.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pt.samoisolationcore.config.security.CustomUserDetails;
import pt.samoisolationcore.domain.Like;
import pt.samoisolationcore.repository.HubRepository;
import pt.samoisolationcore.repository.LikeRepository;
import pt.samoisolationcore.service.LikeService;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Service
public class LikeServiceImpl implements LikeService {
    private static final Logger log = LoggerFactory.getLogger(LikeService.class);
    private final LikeRepository likeRepository;
    private final HubRepository hubRepository;

    public LikeServiceImpl(LikeRepository likeRepository, HubRepository hubRepository) {
        this.likeRepository = likeRepository;
        this.hubRepository = hubRepository;
    }

    @Override
    public List<Like> getUserLikes(@NotNull Integer page) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) {
            throw new AuthorizationServiceException("need to authorize");
        }

        CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
        Page<Like> likePage = likeRepository.findByUserId(userDetails.getUserId(), PageRequest.of(page, LikeService.PAGE_SIZE));
        int totalPages = likePage.getTotalPages();
        if (page >= totalPages) {
            return null;
        }

        return likePage.getContent();
    }

    @Override
    public void toggleLike(@NotBlank String hubId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) {
            throw new AuthorizationServiceException("need to authorize");
        }

        CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
        Optional<Like> optionalLike = likeRepository.findByHubIdAndUserId(hubId, userDetails.getUserId());
        //todo in one transaction
        if (optionalLike.isPresent()) {
            likeRepository.deleteById(optionalLike.get().getId());
            hubRepository.decLikesCounter(hubId);
        } else {
            likeRepository.save(new Like(userDetails.getUserId(), hubId));
            hubRepository.incLikesCounter(hubId);
        }
    }
}
