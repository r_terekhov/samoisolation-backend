package pt.samoisolationcore.service.impl;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.samoisolationcore.repository.*;
import pt.samoisolationcore.service.InitService;

import java.util.Random;

@Profile("local")
@Service
public class InitServiceLocal implements InitService {

    private final UserRoleRepository userRoleRepository;
    private final UserRepository userRepository;
    private final HubCategoryRepository hubCategoryRepository;
    private final HubRepository hubRepository;
    private final CommentRepository commentRepository;
    private final LikeRepository likeRepository;

    public InitServiceLocal(UserRoleRepository userRoleRepository, UserRepository userRepository, HubCategoryRepository hubCategoryRepository, HubRepository hubRepository, CommentRepository commentRepository, LikeRepository likeRepository) {
        this.userRoleRepository = userRoleRepository;
        this.userRepository = userRepository;
        this.hubCategoryRepository = hubCategoryRepository;
        this.hubRepository = hubRepository;
        this.commentRepository = commentRepository;
        this.likeRepository = likeRepository;
    }


    @Override
    public void initDataBase() {
        /*User user = new User("fedya", "fedya123", 21, 1, List.of(userRoleRepository.findUserRoleByName("USER").get()));
        User savedUser = userRepository.save(user);

        HubCategory filmsCategory = new HubCategory("Фильмы", "films", "Какое-то большое описание категории с фильмами", "\uD83C\uDFA5", 5, "https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/QLQoQm9crBM.jpg?alt=media&token=76ff2675-a94b-4a6c-a07d-0d03ac24357d");
        HubCategory gamesCategory = new HubCategory("Игры", "games", "Какое-то большое описание категории с играми", "\uD83C\uDFAE", 5, "https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/QLQoQm9crBM.jpg?alt=media&token=76ff2675-a94b-4a6c-a07d-0d03ac24357d");
        HubCategory savedFilmCategory = hubCategoryRepository.save(filmsCategory);
        HubCategory savedGameCategory = hubCategoryRepository.save(gamesCategory);

        List<String> hubInFilmsIds = new ArrayList<>();
        List<String> hubInGamesIds = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            Hub hubInFilmsCategory = new Hub(savedUser.getId(), "Контент хаба в категории фильмы" + i, Set.of(savedFilmCategory));
            hubInFilmsCategory.setLikesCounter(Long.valueOf(getRandomNumberInRange(0, 20)));
            hubInFilmsCategory.setInternalRating(Long.valueOf(getRandomNumberInRange(0, 20)));
            hubInFilmsIds.add(hubRepository.save(hubInFilmsCategory).getId());

            Hub hubInGamesCategory = new Hub(savedUser.getId(), "Контент хаба в категории games" + i, Set.of(savedGameCategory));
            hubInGamesCategory.setLikesCounter(Long.valueOf(getRandomNumberInRange(0, 20)));
            hubInGamesCategory.setInternalRating(Long.valueOf(getRandomNumberInRange(0, 20)));
            hubInGamesIds.add(hubRepository.save(hubInGamesCategory).getId());
        }

        for (int i = 0; i < hubInFilmsIds.size(); i++) {
            for (int j = 0; j < 15; j++) {
                Comment comment = new Comment(savedUser.getId(), hubInFilmsIds.get(i), format("Коммент для хаба c фильмами %s номер %s", hubInFilmsIds.get(i), j));
                commentRepository.save(comment);
            }
        }

        for (int i = 0; i < hubInGamesIds.size(); i++) {
            for (int j = 0; j < 15; j++) {
                Comment comment = new Comment(savedUser.getId(), hubInFilmsIds.get(i), format("Коммент для хаба c играми %s номер %s", hubInGamesIds.get(i), j));
                commentRepository.save(comment);
            }
        }*/
    }

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}
