package pt.samoisolationcore.service.impl;


import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pt.samoisolationcore.service.InitService;

@Profile("prod")
@Service
public class InitServiceProd implements InitService {

    @Override
    public void initDataBase() {

    }
}
