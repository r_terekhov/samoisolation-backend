package pt.samoisolationcore.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pt.samoisolationcore.config.security.CustomUserDetails;
import pt.samoisolationcore.domain.Document;
import pt.samoisolationcore.domain.Hub;
import pt.samoisolationcore.domain.HubCategory;
import pt.samoisolationcore.domain.User;
import pt.samoisolationcore.dto.CommentDto;
import pt.samoisolationcore.dto.CreateHubRequestDto;
import pt.samoisolationcore.dto.HubDto;
import pt.samoisolationcore.repository.HubCategoryRepository;
import pt.samoisolationcore.repository.HubRepository;
import pt.samoisolationcore.repository.UserRepository;
import pt.samoisolationcore.service.CommentService;
import pt.samoisolationcore.service.HubService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.text.MessageFormat.format;

@Service
public class HubServiceImpl implements HubService {
    private static final Logger log = LoggerFactory.getLogger(HubService.class);
    private final HubRepository hubRepository;
    private final CommentService commentsService;
    private final HubCategoryRepository hubCategoryRepository;
    private final UserRepository userRepository;


    public HubServiceImpl(HubRepository hubRepository, CommentService commentsService, HubCategoryRepository hubCategoryRepository, UserRepository userRepository) {
        this.hubRepository = hubRepository;
        this.commentsService = commentsService;
        this.hubCategoryRepository = hubCategoryRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<HubDto> getHubs(@NotNull Integer page, @NotBlank String sort) {
        Page<Hub> hubPage;
        switch (sort) {
            case "likesCount": {
                hubPage = hubRepository.findAllByOrderByLikesCounterDesc(PageRequest.of(page, PAGE_SIZE));
                break;
            }
            case "newFirst": {
                hubPage = hubRepository.findAllByOrderByCreatedDesc(PageRequest.of(page, PAGE_SIZE));
                break;
            }

            case "smartLikesAndComment": {
                hubPage = hubRepository.findAllByOrderByInternalRatingDesc(PageRequest.of(page, PAGE_SIZE));
                break;
            }

            default:
                throw new RuntimeException("unsupported sort option");
        }

        int totalPages = hubPage.getTotalPages();
        if (page >= totalPages) {
            return null;
        }

        return hubPage.get().map(hub -> {
            List<CommentDto> comments = commentsService.getComments(hub.getId(), page, CommentService.PAGE_SIZE);
            return new HubDto(
                    hub.getId(),
                    hub.getUserId(),
                    hub.getAuthorNick(),
                    hub.getAuthorAge(),
                    hub.getAuthorGender(),
                    hub.getContent(),
                    hub.getLikesCounter(),
                    hub.getCommentsCounter(),
                    hub.getCategories(),
                    comments
            );
        }).collect(Collectors.toList());
    }

    @Override
    public List<HubDto> getHubsByCategories(@NotNull Integer page, @NotBlank String categories, @NotBlank String sort) {
        Set<String> hubCategories = getHubCategoriesByCategoriesInString(categories);
        if (hubCategories == null) {
            return null;
        }

        Page<Hub> hubPage;
        switch (sort) {
            case "likesCount": {
                hubPage = hubRepository.findAllByCategoriesInOrderByLikesCounterDesc(hubCategories, PageRequest.of(page, PAGE_SIZE));
                break;
            }
            case "newFirst": {
                hubPage = hubRepository.findAllByCategoriesInOrderByCreatedDesc(hubCategories, PageRequest.of(page, PAGE_SIZE));
                break;
            }

            case "smartLikesAndComment": {
                hubPage = hubRepository.findAllByCategoriesInOrderByInternalRatingDesc(hubCategories, PageRequest.of(page, PAGE_SIZE));
                break;
            }

            default:
                throw new RuntimeException("unsupported sort option");
        }

        int totalPages = hubPage.getTotalPages();
        if (page >= totalPages) {
            return null;
        }

        return hubPage.get().map(hub -> {
            List<CommentDto> comments = commentsService.getComments(hub.getId(), page, CommentService.PAGE_SIZE);
            return new HubDto(
                    hub.getId(),
                    hub.getUserId(),
                    hub.getAuthorNick(),
                    hub.getAuthorAge(),
                    hub.getAuthorGender(),
                    hub.getContent(),
                    hub.getLikesCounter(),
                    hub.getCommentsCounter(),
                    hub.getCategories(),
                    comments
            );
        }).collect(Collectors.toList());
    }

    @Override
    public HubDto createHub(@Valid CreateHubRequestDto createHubRequestDto) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) {
            throw new AuthorizationServiceException("need to authorize");
        }

        CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
        Set<String> hubCategories = getHubCategoriesByCategoriesInString(createHubRequestDto.getCategories());
        User user = userRepository.findById(userDetails.getUserId()).orElseThrow(() -> new RuntimeException(format("user not found by id {0} on creating hub", userDetails.getUserId())));
        return HubDto.of(hubRepository.save(new Hub(
                user.getId(),
                user.getNick(),
                user.getAge(),
                user.getGender(),
                createHubRequestDto.getContent(),
                hubCategories
        )), null);
    }

    @Override
    public HubDto getHub(@NotBlank String hubId) {
        Optional<Hub> optionalHub = hubRepository.findById(hubId);
        if (!optionalHub.isPresent()) {
            return null;
        }

        Hub hub = optionalHub.get();
        List<CommentDto> comments = commentsService.getComments(hubId, 0, CommentService.PAGE_SIZE);
        return HubDto.of(hub, comments);

    }

    private Set<String> getHubCategoriesByCategoriesInString(String rawCategories) {
        Set<String> stringCategories = Set.of(rawCategories.split(","));

        if (stringCategories.isEmpty()) {
            return null;
        }

        Set<HubCategory> hubCategories = hubCategoryRepository.findAllByCategoryIn(stringCategories);
        return hubCategories.stream()
                .map(Document::getId)
                .collect(Collectors.toSet());
    }
}
