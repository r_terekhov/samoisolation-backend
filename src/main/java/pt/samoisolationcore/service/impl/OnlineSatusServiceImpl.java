package pt.samoisolationcore.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pt.samoisolationcore.domain.OnlineStatus;
import pt.samoisolationcore.properties.Properties;
import pt.samoisolationcore.repository.OnlineStatusRepository;
import pt.samoisolationcore.service.OnlineStatusService;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Service
public class OnlineSatusServiceImpl implements OnlineStatusService {
    private static final Logger log = LoggerFactory.getLogger(OnlineStatusService.class);
    private final OnlineStatusRepository onlineStatusRepository;
    private final Properties properties;
    private static final Long initOnlineValue = 25L;

    public OnlineSatusServiceImpl(OnlineStatusRepository onlineStatusRepository, Properties properties) {
        this.onlineStatusRepository = onlineStatusRepository;
        this.properties = properties;
    }

    @Override
    public Long getCurrentOnline() {
        Long realOnline = onlineStatusRepository.countAllBy();
        log.info("real online is {}", realOnline);
        return initOnlineValue + realOnline;
    }

    @Override
    public void updateInfoAboutUserOnline(String ip) {
        Optional<OnlineStatus> onlineStatusByAnonymousUser = onlineStatusRepository.findOnlineStatusByAnonymousUser(ip);
        Instant newExpireAt = Instant.now().plus(properties.getOnlineStatusTtl(), ChronoUnit.MINUTES);
        if (onlineStatusByAnonymousUser.isPresent()) {
            onlineStatusByAnonymousUser.get().setExpireAt(newExpireAt);
            onlineStatusRepository.save(onlineStatusByAnonymousUser.get());
        } else {
            OnlineStatus onlineStatus = new OnlineStatus(newExpireAt, ip);
            onlineStatusRepository.save(onlineStatus);
        }
    }
}
