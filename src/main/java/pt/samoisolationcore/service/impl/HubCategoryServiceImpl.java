package pt.samoisolationcore.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pt.samoisolationcore.domain.HubCategory;
import pt.samoisolationcore.dto.HubCategoryDto;
import pt.samoisolationcore.repository.HubCategoryRepository;
import pt.samoisolationcore.service.HubCategoryService;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HubCategoryServiceImpl implements HubCategoryService {
    private static final Logger log = LoggerFactory.getLogger(HubCategoryService.class);
    private final HubCategoryRepository hubCategoryRepository;

    public HubCategoryServiceImpl(HubCategoryRepository hubCategoryRepository) {
        this.hubCategoryRepository = hubCategoryRepository;
    }


    @Override
    public List<HubCategoryDto> getHubCategories(@NotNull Integer page) {
        Page<HubCategory> hubCategoryPage = hubCategoryRepository.findAllByOrderByHotRatingDesc(PageRequest.of(page, PAGE_SIZE));
        int totalPages = hubCategoryPage.getTotalPages();
        if (page >= totalPages) {
            return null;
        }

        return hubCategoryPage
                .stream()
                .map(HubCategoryDto::of)
                .collect(Collectors.toList());
    }
}
