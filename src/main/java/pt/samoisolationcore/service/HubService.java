package pt.samoisolationcore.service;

import pt.samoisolationcore.dto.CreateHubRequestDto;
import pt.samoisolationcore.dto.HubDto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public interface HubService {
    int PAGE_SIZE = 10;

    List<HubDto> getHubs(@NotNull Integer page, @NotBlank String sort);

    List<HubDto> getHubsByCategories(@NotNull Integer page, @NotBlank String categories, @NotBlank String sort);

    HubDto createHub(@Valid CreateHubRequestDto createHubRequestDto);

    HubDto getHub(@NotBlank String hubId);
}
