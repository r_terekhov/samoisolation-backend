package pt.samoisolationcore.service;

public interface OnlineStatusService {

    Long getCurrentOnline();

    void updateInfoAboutUserOnline(String ip);
}
