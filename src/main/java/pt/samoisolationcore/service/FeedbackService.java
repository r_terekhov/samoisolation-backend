package pt.samoisolationcore.service;

import pt.samoisolationcore.domain.Feedback;
import pt.samoisolationcore.dto.GiveFeedbackDto;

import javax.validation.Valid;

public interface FeedbackService {

    Feedback giveFeedback(@Valid GiveFeedbackDto giveFeedbackDto);
}
