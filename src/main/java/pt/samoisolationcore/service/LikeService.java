package pt.samoisolationcore.service;

import pt.samoisolationcore.domain.Like;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public interface LikeService {
    int PAGE_SIZE = 10;

    List<Like> getUserLikes(@NotNull Integer page);

    void toggleLike(@NotBlank String hubId);
}
