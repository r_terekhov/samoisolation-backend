package pt.samoisolationcore.service;

import pt.samoisolationcore.dto.CommentDto;
import pt.samoisolationcore.dto.CreateCommentRequestDto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public interface CommentService {
    int PAGE_SIZE = 10;

    List<CommentDto> getComments(@NotBlank String hubId, @NotNull Integer page, @NotNull Integer commentsPageSize);

    CommentDto createComment(@Valid CreateCommentRequestDto createCommentRequestDto);
}
