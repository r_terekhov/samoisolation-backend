package pt.samoisolationcore.service;

import pt.samoisolationcore.dto.HubCategoryDto;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface HubCategoryService {
    int PAGE_SIZE = 10;

    List<HubCategoryDto> getHubCategories(@NotNull Integer page);
}
