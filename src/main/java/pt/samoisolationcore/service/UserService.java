package pt.samoisolationcore.service;

import pt.samoisolationcore.dto.UserDto;
import pt.samoisolationcore.dto.UserRegistrationRequestDto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

public interface UserService {

    Boolean checkIfUserExists(@NotBlank String nick);

    UserDto register(@Valid UserRegistrationRequestDto userRegistrationRequestDto);
}
