package pt.samoisolationcore.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pt.samoisolationcore.model.BaseResponse;
import pt.samoisolationcore.properties.Properties;

import static java.text.MessageFormat.format;

@SuppressWarnings("rawtypes")
@RestController
@Validated
public class HealthServicesController {
    private static final Logger log = LoggerFactory.getLogger(HealthServicesController.class);
    private final Properties properties;

    public HealthServicesController(Properties properties) {
        this.properties = properties;
    }

    @GetMapping("health")
    public ResponseEntity<String> health() {
        return new ResponseEntity<>(format("OK, instance {0}", properties.getAppName()), HttpStatus.OK);
    }


    @ExceptionHandler(AuthorizationServiceException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody
    ResponseEntity<BaseResponse> onUnauthorized(AuthorizationServiceException ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
    }


    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseResponse> onInvalidRequest(Exception ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
