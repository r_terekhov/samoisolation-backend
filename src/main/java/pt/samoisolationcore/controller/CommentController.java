package pt.samoisolationcore.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pt.samoisolationcore.dto.CommentDto;
import pt.samoisolationcore.dto.CreateCommentRequestDto;
import pt.samoisolationcore.model.BaseResponse;
import pt.samoisolationcore.service.CommentService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@Validated
@RequestMapping("core/comment")
public class CommentController {
    private static final Logger log = LoggerFactory.getLogger(CommentController.class);
    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping(value = "create")
    public ResponseEntity<BaseResponse<CommentDto>> create(
            @Valid @RequestBody CreateCommentRequestDto createCommentRequestDto) {
        return new ResponseEntity<>(BaseResponse.response(commentService.createComment(createCommentRequestDto)), HttpStatus.CREATED);

    }

    @GetMapping("getComments")
    public ResponseEntity<BaseResponse<List<CommentDto>>> getComments(
            @NotBlank @RequestParam String hubId,
            @NotNull @RequestParam Integer page) {
        return new ResponseEntity<>(BaseResponse.response(commentService.getComments(hubId, page, CommentService.PAGE_SIZE)), HttpStatus.OK);
    }


    @ExceptionHandler(AuthorizationServiceException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody
    ResponseEntity<BaseResponse> onUnauthorized(AuthorizationServiceException ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseResponse> onInvalidRequest(Exception ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
