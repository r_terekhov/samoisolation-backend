package pt.samoisolationcore.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pt.samoisolationcore.dto.CreateHubRequestDto;
import pt.samoisolationcore.dto.HubDto;
import pt.samoisolationcore.model.BaseResponse;
import pt.samoisolationcore.service.HubService;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@Validated
@RequestMapping("core/hub/")
public class HubController {
    private static final Logger log = LoggerFactory.getLogger(HubController.class);
    private final HubService hubService;

    public HubController(HubService hubService) {
        this.hubService = hubService;
    }


    @GetMapping(value = "getHub")
    public ResponseEntity<BaseResponse<HubDto>> getHub(
            @NotBlank @RequestParam String hubId) {
        return new ResponseEntity<>(BaseResponse.response(hubService.getHub(hubId)), HttpStatus.OK);
    }

    @GetMapping(value = "getHubs")
    public ResponseEntity<BaseResponse<List<HubDto>>> getHubs(
            @NotNull @RequestParam Integer page,
            @NotBlank @RequestParam String sort) {
        return new ResponseEntity<>(BaseResponse.response(hubService.getHubs(page, sort)), HttpStatus.OK);
    }

    @GetMapping(value = "getHubsWithCategoriesIn")
    public ResponseEntity<BaseResponse<List<HubDto>>> getHubsWithCategoriesIn(
            @NotNull @RequestParam Integer page,
            @NotBlank @RequestParam String categories,
            @NotBlank @RequestParam String sort) {
        return new ResponseEntity<>(BaseResponse.response(hubService.getHubsByCategories(page, categories, sort)), HttpStatus.OK);

    }

    @PostMapping(value = "create")
    public ResponseEntity<BaseResponse<HubDto>> create(
            @Valid @RequestBody CreateHubRequestDto createHubRequestDto) {
        return new ResponseEntity<>(BaseResponse.response(hubService.createHub(createHubRequestDto)), HttpStatus.CREATED);
    }


    @ExceptionHandler(AuthorizationServiceException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody
    ResponseEntity<BaseResponse> onUnauthorized(AuthorizationServiceException ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseResponse> onInvalidRequest(Exception ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
