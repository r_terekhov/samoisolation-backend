package pt.samoisolationcore.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pt.samoisolationcore.domain.Like;
import pt.samoisolationcore.model.BaseResponse;
import pt.samoisolationcore.service.LikeService;

import javax.validation.constraints.NotNull;
import java.util.List;

@SuppressWarnings("rawtypes")
@RestController
@Validated
@RequestMapping("core/like")
public class LikeSystemController {
    private static final Logger log = LoggerFactory.getLogger(LikeSystemController.class);
    private final LikeService likeService;

    public LikeSystemController(LikeService likeService) {
        this.likeService = likeService;
    }


    @GetMapping(value = "getUserLikes")
    public ResponseEntity<BaseResponse<List<Like>>> getUserLikes(@NotNull @RequestParam Integer page) {
        return new ResponseEntity<>(BaseResponse.response(likeService.getUserLikes(page)), HttpStatus.OK);
    }

    @PostMapping(value = "toggle")
    public ResponseEntity toggle(
            @RequestParam String hubId) {
        likeService.toggleLike(hubId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ExceptionHandler(AuthorizationServiceException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody
    ResponseEntity<BaseResponse> onUnauthorized(AuthorizationServiceException ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseResponse> onInvalidRequest(Exception ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
