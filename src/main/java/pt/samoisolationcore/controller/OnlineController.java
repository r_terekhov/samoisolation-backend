package pt.samoisolationcore.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.web.bind.annotation.*;
import pt.samoisolationcore.model.BaseResponse;
import pt.samoisolationcore.service.OnlineStatusService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("core/status")
public class OnlineController {
    private static final Logger log = LoggerFactory.getLogger(OnlineController.class);
    private final OnlineStatusService onlineStatusService;

    public OnlineController(OnlineStatusService onlineStatusService) {
        this.onlineStatusService = onlineStatusService;
    }


    @GetMapping("online")
    public Long getOnline(HttpServletRequest request) {
        String clientIpAddress = getClientIpAddress(request);
        onlineStatusService.updateInfoAboutUserOnline(clientIpAddress);
        return onlineStatusService.getCurrentOnline();
    }

    private static final String[] IP_HEADER_CANDIDATES = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR"};

    public static String getClientIpAddress(HttpServletRequest request) {
        for (String header : IP_HEADER_CANDIDATES) {
            String ip = request.getHeader(header);
            if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
                return ip;
            }
        }
        return request.getRemoteAddr();
    }

    @ExceptionHandler(AuthorizationServiceException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody
    ResponseEntity<BaseResponse> onUnauthorized(AuthorizationServiceException ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseResponse> onInvalidRequest(Exception ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
