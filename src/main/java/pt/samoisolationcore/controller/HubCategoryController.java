package pt.samoisolationcore.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pt.samoisolationcore.dto.HubCategoryDto;
import pt.samoisolationcore.model.BaseResponse;
import pt.samoisolationcore.service.HubCategoryService;

import javax.validation.constraints.NotNull;
import java.util.List;

@SuppressWarnings("rawtypes")
@RestController
@Validated
@RequestMapping("core/hubcategory/")
public class HubCategoryController {
    private static final Logger log = LoggerFactory.getLogger(HubCategoryController.class);
    private final HubCategoryService hubCategoryService;

    public HubCategoryController(HubCategoryService hubCategoryService) {
        this.hubCategoryService = hubCategoryService;
    }


    @GetMapping(value = "getCategories")
    public ResponseEntity<BaseResponse<List<HubCategoryDto>>> getHubCategories(
            @NotNull @RequestParam Integer page) {
        return new ResponseEntity<>(BaseResponse.response(hubCategoryService.getHubCategories(page)), HttpStatus.OK);
    }


    @ExceptionHandler(AuthorizationServiceException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody
    ResponseEntity<BaseResponse> onUnauthorized(AuthorizationServiceException ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseResponse> onInvalidRequest(Exception ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
