package pt.samoisolationcore.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pt.samoisolationcore.dto.UserDto;
import pt.samoisolationcore.dto.UserRegistrationRequestDto;
import pt.samoisolationcore.model.BaseResponse;
import pt.samoisolationcore.service.UserService;

import javax.validation.Valid;

import static java.text.MessageFormat.format;

@RestController
@Validated
@RequestMapping("core/user")
public class UserController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;
    private final AuthenticationManager authenticationManager;

    public UserController(UserService userService, AuthenticationManager authenticationManager) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
    }

    @GetMapping(value = "/checkIfExists")
    public ResponseEntity<BaseResponse<Boolean>> checkIfExists(@RequestParam String nick) {
        return new ResponseEntity<>(BaseResponse.response(userService.checkIfUserExists(nick)), HttpStatus.OK);
    }


    @PostMapping(value = "/signup")
    public ResponseEntity<BaseResponse<UserDto>> register(
            @Valid @RequestBody UserRegistrationRequestDto userRegistrationRequestDto) {
        if (userService.checkIfUserExists(userRegistrationRequestDto.getNick())) {
            return new ResponseEntity<>(BaseResponse.error(format("User with nick {0} already exists :(, choose another one", userRegistrationRequestDto.getNick())), HttpStatus.CONFLICT);
        }


        UserDto registeredUserDto = userService.register(userRegistrationRequestDto);

    /*    //autologin
    https://stackoverflow.com/questions/44612276/how-to-get-oauth2-access-token-when-user-call-signup-rest-api-in-springboot
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userRegistrationRequestDto.getNick(), userRegistrationRequestDto.getPassword());
        authToken.setDetails(new WebAuthenticationDetails(request));

        Authentication authentication = authenticationManager.authenticate(authToken);

        SecurityContextHolder.getContext().setAuthentication(authentication);*/

        return new ResponseEntity<>(BaseResponse.response(registeredUserDto), HttpStatus.CREATED);
    }


    @ExceptionHandler(AuthorizationServiceException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody
    ResponseEntity<BaseResponse> onUnauthorized(AuthorizationServiceException ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    ResponseEntity<BaseResponse> onInvalidRequest(Exception ex) {
        log.error(ex.getMessage());
        BaseResponse response = BaseResponse.error(ex.getLocalizedMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
