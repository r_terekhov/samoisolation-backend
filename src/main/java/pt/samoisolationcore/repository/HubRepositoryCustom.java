package pt.samoisolationcore.repository;

import pt.samoisolationcore.domain.Hub;

public interface HubRepositoryCustom {

    Hub incLikesCounter(String hubId);

    Hub incCommentsCounter(String hubId);

    Hub decLikesCounter(String hubId);

    Hub decCommentsCounter(String hubId);

}
