package pt.samoisolationcore.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.samoisolationcore.domain.OnlineStatus;

import java.util.Optional;

@Repository
public interface OnlineStatusRepository extends MongoRepository<OnlineStatus, String> {

    Optional<OnlineStatus> findOnlineStatusByAnonymousUser(String ip);

    Long countAllBy();
}
