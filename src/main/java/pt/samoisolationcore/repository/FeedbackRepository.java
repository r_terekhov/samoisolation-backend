package pt.samoisolationcore.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.samoisolationcore.domain.Feedback;

@Repository
public interface FeedbackRepository extends MongoRepository<Feedback, String> {

}
