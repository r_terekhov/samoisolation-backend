package pt.samoisolationcore.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.samoisolationcore.domain.HubCategory;

import java.util.Set;

@Repository
public interface HubCategoryRepository extends MongoRepository<HubCategory, String> {

    Page<HubCategory> findAllByCategoryIn(Set<String> categories, Pageable pageable);

    Set<HubCategory> findAllByCategoryIn(Set<String> categories);

    Page<HubCategory> findAllByOrderByHotRatingDesc(Pageable pageable);
}
