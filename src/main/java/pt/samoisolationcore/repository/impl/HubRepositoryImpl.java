package pt.samoisolationcore.repository.impl;

import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import pt.samoisolationcore.domain.Hub;
import pt.samoisolationcore.repository.HubRepositoryCustom;

public class HubRepositoryImpl implements HubRepositoryCustom {


    private final MongoTemplate mongoTemplate;

    public HubRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }


    @Override
    public Hub incLikesCounter(String hubId) {
        Query query = new Query(Criteria.where("_id").is(hubId));
        Update update = new Update().inc("likesCounter", 1).inc("internalRating", 2);
        return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Hub.class);
    }

    @Override
    public Hub decLikesCounter(String hubId) {
        Query query = new Query(Criteria.where("_id").is(hubId));
        Update update = new Update().inc("likesCounter", -1).inc("internalRating", -2);
        return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Hub.class);
    }

    @Override
    public Hub incCommentsCounter(String hubId) {
        Query query = new Query(Criteria.where("_id").is(hubId));
        Update update = new Update().inc("commentsCounter", 1).inc("internalRating", 3);
        return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Hub.class);
    }

    @Override
    public Hub decCommentsCounter(String hubId) {
        Query query = new Query(Criteria.where("_id").is(hubId));
        Update update = new Update().inc("commentsCounter", -1).inc("internalRating", -3);
        return mongoTemplate.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Hub.class);
    }
}
