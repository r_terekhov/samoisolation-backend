package pt.samoisolationcore.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.samoisolationcore.domain.Hub;

import javax.validation.constraints.NotBlank;
import java.util.Set;

@Repository
public interface HubRepository extends MongoRepository<Hub, String>, HubRepositoryCustom {

    Page<Hub> findAllByCategoriesInOrderByLikesCounterDesc(@NotBlank Set<String> categories, Pageable pageable);

    Page<Hub> findAllByCategoriesInOrderByInternalRatingDesc(@NotBlank Set<String> categories, Pageable pageable);

    Page<Hub> findAllByCategoriesInOrderByCreatedDesc(@NotBlank Set<String> categories, Pageable pageable);

    Page<Hub> findAllByOrderByLikesCounterDesc(Pageable pageable);

    Page<Hub> findAllByOrderByInternalRatingDesc(Pageable pageable);

    Page<Hub> findAllByOrderByCreatedDesc(Pageable pageable);

}
