package pt.samoisolationcore.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.samoisolationcore.domain.Comment;

@Repository
public interface CommentRepository extends MongoRepository<Comment, String> {

    Page<Comment> findAllByHubIdOrderByCreatedAsc(String hubId, Pageable pageable);

}
