package pt.samoisolationcore.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.samoisolationcore.domain.Like;

import java.util.Optional;

@Repository
public interface LikeRepository extends MongoRepository<Like, String> {

    Page<Like> findByUserId(String userId, Pageable pageable);

    Optional<Like> findByHubIdAndUserId(String hubId, String userId);
}
