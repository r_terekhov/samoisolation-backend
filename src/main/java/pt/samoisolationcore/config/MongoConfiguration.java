/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * vk.com/r_terekhov
 * Copyright (c) 2020.
 */

package pt.samoisolationcore.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;


@Configuration
@EnableMongoRepositories(basePackages = {"pt.samoisolationcore.repository"})
@EnableAutoConfiguration
@Slf4j
public class MongoConfiguration {

    private final MongoTemplate mongoTemplate;

    private final MongoConverter mongoConverter;

    public MongoConfiguration(MongoTemplate mongoTemplate, MongoConverter mongoConverter) {
        this.mongoTemplate = mongoTemplate;
        this.mongoConverter = mongoConverter;
    }

//    @EventListener(ApplicationReadyEvent.class)
//    public void initIndicesAfterStartup() {
//
//        log.info("Mongo InitIndicesAfterStartup init");
//
//        MappingContext<? extends MongoPersistentEntity<?>, MongoPersistentProperty> mappingContext = this.mongoConverter.getMappingContext();
//
//        if (mappingContext instanceof MongoMappingContext) {
//            MongoMappingContext mongoMappingContext = (MongoMappingContext) mappingContext;
//            for (BasicMongoPersistentEntity<?> persistentEntity : mongoMappingContext.getPersistentEntities()) {
//                Class<?> clazz = persistentEntity.getType();
//                if (clazz.isAnnotationPresent(Document.class)) {
//                    MongoPersistentEntityIndexResolver resolver = new MongoPersistentEntityIndexResolver(mongoMappingContext);
//
//                    IndexOperations indexOps = mongoTemplate.indexOps(clazz);
//                    resolver.resolveIndexFor(clazz).forEach(indexOps::ensureIndex);
//                }
//            }
//        }
//    }

}