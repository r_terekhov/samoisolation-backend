/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package pt.samoisolationcore.config;

import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.mongodb.core.MongoTemplate;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@Configuration
@DependsOn("mongoTemplate")
public class CollectionsConfig {

    private final MongoTemplate mongoTemplate;

    public CollectionsConfig(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @PostConstruct
    public void initIndexes() {
        //ttl
        mongoTemplate.getCollection("onlineStatus").createIndex(Indexes.ascending("expireAt"),
                new IndexOptions().expireAfter(0L, TimeUnit.SECONDS));

    }


}