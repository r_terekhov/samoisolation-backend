package pt.samoisolationcore.config.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        //-- define URL patterns to enable OAuth2 security
        http.
                anonymous().disable()
                .requestMatchers().antMatchers("/**")
                .and().authorizeRequests()
                .antMatchers("/core/comment/create").access("hasRole('ADMIN') or hasRole('USER')")
                .antMatchers("/core/hub/create").access("hasRole('ADMIN') or hasRole('USER')")
                .antMatchers("/core/like/getUserLikes").access("hasRole('ADMIN') or hasRole('USER')")
                .antMatchers("/core/like/toggle").access("hasRole('ADMIN') or hasRole('USER')")

                .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
    }
}
