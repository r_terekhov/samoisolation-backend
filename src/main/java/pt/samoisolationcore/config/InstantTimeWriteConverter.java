/*
 * Roman Terekhov
 * terekhov_roman@mail.ru
 * Copyright (c) 2019.
 */

package pt.samoisolationcore.config;


import org.springframework.core.convert.converter.Converter;

import java.time.Instant;
import java.util.Date;

public class InstantTimeWriteConverter implements Converter<Instant, Date> {
    @Override
    public Date convert(Instant instant) {
        return Date.from(instant);
    }
}