package pt.samoisolationcore.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import pt.samoisolationcore.domain.UserRole;
import pt.samoisolationcore.model.Roles;
import pt.samoisolationcore.repository.UserRoleRepository;
import pt.samoisolationcore.service.InitService;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {
    private final UserRoleRepository userRoleRepository;
    private final InitService initService;


    @Autowired
    public ApplicationStartup(UserRoleRepository userRoleRepository, InitService initService) {
        this.userRoleRepository = userRoleRepository;
        this.initService = initService;
    }

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {

        // here your code ...
        addRolesIfTheyNotExists();
        //for testing only
        //initService.initDataBase();

    }


    private void addRolesIfTheyNotExists() {
        if (!userRoleRepository.findUserRoleByName(Roles.USER.name()).isPresent()) {
            userRoleRepository.save(new UserRole(Roles.USER.name()));
        }

        if (!userRoleRepository.findUserRoleByName(Roles.ADMIN.name()).isPresent()) {
            userRoleRepository.save(new UserRole(Roles.ADMIN.name()));
        }
    }
}
