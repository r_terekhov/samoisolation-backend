package pt.samoisolationcore.domain;

import lombok.Getter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.Version;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@org.springframework.data.mongodb.core.mapping.Document
public abstract class Document {
    @Transient
    private static final Double CURRENT_SCHEMA_VERSION = 1.0;

    @Id
    @Getter
    @NotBlank
    private String id;


    @Getter
    @CreatedDate
    @NotNull
    private Instant created;

    @Version
    @Getter
    @NotNull
    private Integer version;

    @Getter
    @NotNull
    private Double schemaVersion = CURRENT_SCHEMA_VERSION;

    public Document() {
        this.created = ZonedDateTime.now(ZoneId.of("UTC")).toInstant();
    }


}
