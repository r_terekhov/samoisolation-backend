package pt.samoisolationcore.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class User extends Document {
    @Indexed
    @NotBlank
    private final String nick;

    @NotBlank
    private final String password;

    @NotNull
    private final Integer age;

    /**
     * 0 - woman
     * 1 - man
     * 2 - hui znaet kto
     * 3 - not setupped
     */
    @Getter
    @NotNull
    private final Integer gender;

    @NotNull
    private final List<UserRole> roles;


    public User(@NotBlank String nick, @NotBlank String password, @NotNull Integer age, @NotNull Integer gender, @NotNull List<UserRole> roles) {
        this.nick = nick;
        this.password = password;
        this.age = age;
        this.gender = gender;
        this.roles = roles;
    }
}
