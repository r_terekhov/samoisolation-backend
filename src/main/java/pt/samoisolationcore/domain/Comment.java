package pt.samoisolationcore.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class Comment extends Document {

    @NotBlank
    @Indexed
    private final String userId;

    @NotBlank
    private final String authorNick;

    @NotNull
    private final Integer authorAge;

    @NotNull
    private final Integer authorGender;

    @NotBlank
    @Indexed
    private final String hubId;

    @NotBlank
    private final String content;





}
