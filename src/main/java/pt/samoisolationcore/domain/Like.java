package pt.samoisolationcore.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotBlank;

@EqualsAndHashCode(callSuper = true)
@Data
public class Like extends Document {

    @Indexed
    @NotBlank
    private final String userId;

    @Indexed
    @NotBlank
    private final String hubId;


}
