package pt.samoisolationcore.domain;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotBlank;
import java.time.Instant;

@EqualsAndHashCode(callSuper = false)
@Value
public class OnlineStatus extends ExpiredDocument {

    @Indexed
    @NotBlank String anonymousUser;


    public OnlineStatus(Instant expireAt, @NotBlank String anonymousUser) {
        super(expireAt);
        this.anonymousUser = anonymousUser;
    }
}
