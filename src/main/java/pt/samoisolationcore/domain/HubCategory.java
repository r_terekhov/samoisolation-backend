package pt.samoisolationcore.domain;

import lombok.Getter;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

public class HubCategory extends Document {

    @NotBlank
    @Getter
    private final String name;

    @Indexed
    @NotBlank
    @Getter
    private final String category;

    @NotBlank
    @Getter
    private final String description;

    @NotBlank
    @Getter
    private final String emoji;


    /**
     * 0 - not hot
     * 5 - very hot
     */
    @Indexed
    @NotNull
    @Getter
    private Integer hotRating;

    @NotBlank
    @Getter
    private final String url;

    public HubCategory(@NotBlank String name, @NotBlank String category, @NotBlank String description, String emoji, Integer hotRating, String url) {
        this.name = name;
        this.category = category;
        this.description = description;
        this.emoji = emoji;
        this.hotRating = hotRating;
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HubCategory)) return false;
        HubCategory that = (HubCategory) o;
        return category.equals(that.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(category);
    }
}
