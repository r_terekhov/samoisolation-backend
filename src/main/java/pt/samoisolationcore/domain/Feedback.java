package pt.samoisolationcore.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
public class Feedback extends Document {

    /**
     * 0 - bad
     * 5 - excellent
     */
    @NotNull
    Integer rating;

    @Nullable
    String content;

}
