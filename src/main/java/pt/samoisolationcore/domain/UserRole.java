package pt.samoisolationcore.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@EqualsAndHashCode(callSuper = false)
@Data
@NoArgsConstructor
public class UserRole extends Document {
    private @NotBlank String name;

    public UserRole(String name) {
        super();
        this.name = name;
    }

}