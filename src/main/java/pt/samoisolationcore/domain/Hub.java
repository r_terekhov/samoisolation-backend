package pt.samoisolationcore.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@EqualsAndHashCode(callSuper = false)
@Data
public class Hub extends Document {

    @Indexed
    @NotBlank
    private final String userId;

    @NotBlank
    private final String authorNick;

    @NotNull
    private final Integer authorAge;

    @NotNull
    private final Integer authorGender;

    @NotBlank
    private final String content;

    @Indexed
    @NotBlank
    private final Set<String> categories;

    @NotNull
    private Long commentsCounter;

    @Indexed
    @NotNull
    private Long likesCounter;

    @Indexed
    @NotNull
    private Long internalRating;

    public Hub(@NotBlank String userId, @NotBlank String authorNick, @NotNull Integer authorAge, @NotNull Integer authorGender, @NotBlank String content, @NotBlank Set<String> categories) {
        this.userId = userId;
        this.authorNick = authorNick;
        this.authorAge = authorAge;
        this.authorGender = authorGender;
        this.content = content;
        this.categories = categories;
        this.likesCounter = 0L;
        this.commentsCounter = 0L;
        this.internalRating = 0L;
    }

}
