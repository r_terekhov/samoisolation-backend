package pt.samoisolationcore.dto;

import lombok.Value;
import pt.samoisolationcore.domain.HubCategory;

import javax.validation.constraints.NotBlank;

@Value
public class HubCategoryDto {

    @NotBlank String categoryId;

    @NotBlank String name;

    @NotBlank String category;

    @NotBlank String description;

    @NotBlank String emoji;

    @NotBlank String url;

    public static HubCategoryDto of(HubCategory hubCategory) {
        return new HubCategoryDto(
                hubCategory.getId(),
                hubCategory.getName(),
                hubCategory.getCategory(),
                hubCategory.getDescription(),
                hubCategory.getEmoji(),
                hubCategory.getUrl()
        );
    }

}
