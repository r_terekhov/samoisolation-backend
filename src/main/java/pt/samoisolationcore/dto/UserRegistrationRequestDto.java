package pt.samoisolationcore.dto;

import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Value
public class UserRegistrationRequestDto {
    @NotBlank String nick;

    @NotBlank String password;

    @NotNull Integer age;

    @NotNull Integer gender;
}
