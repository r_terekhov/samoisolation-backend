package pt.samoisolationcore.dto;

import lombok.Value;
import org.springframework.lang.Nullable;
import pt.samoisolationcore.domain.Hub;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Value
public class HubDto {

    @NotBlank
    String hubId;

    @NotBlank
    String authorId;

    @NotBlank
    String authorNick;

    @NotNull
    Integer authorAge;

    @NotNull
    Integer authorGender;

    @NotBlank
    String content;

    @NotNull
    Long likesCounter;

    @NotNull
    Long commentsCounter;

    @NotNull
    Set<String> hubCategories;


    @Nullable
    List<CommentDto> comments;

    public static HubDto of(Hub hub, List<CommentDto> comments) {
        return new HubDto(
                hub.getId(),
                hub.getUserId(),
                hub.getAuthorNick(),
                hub.getAuthorAge(),
                hub.getAuthorGender(),
                hub.getContent(),
                hub.getLikesCounter(),
                hub.getCommentsCounter(),
                hub.getCategories(),
                comments
        );
    }
}
