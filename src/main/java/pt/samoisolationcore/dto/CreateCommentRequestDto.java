package pt.samoisolationcore.dto;

import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
public class CreateCommentRequestDto {

    @NotBlank String hubId;

    @NotBlank String content;
}
