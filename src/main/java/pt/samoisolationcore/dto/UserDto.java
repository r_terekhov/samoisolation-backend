package pt.samoisolationcore.dto;

import lombok.Value;
import pt.samoisolationcore.domain.User;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Value
public class UserDto {

    @NotBlank String userId;

    @NotBlank String nick;

    /*   @NotBlank String password;*/

    @NotNull Integer age;

    @NotNull Integer gender;

    public static UserDto of(User user) {
        return new UserDto(
                user.getId(),
                user.getNick(),
                user.getAge(),
                user.getGender()
        );
    }
}
