package pt.samoisolationcore.dto;

import lombok.Value;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;

@Value
public class GiveFeedbackDto {

    @NotNull
    Integer rating;

    @Nullable
    String content;

}
