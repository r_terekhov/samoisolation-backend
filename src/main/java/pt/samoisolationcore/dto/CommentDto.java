package pt.samoisolationcore.dto;

import lombok.Value;
import pt.samoisolationcore.domain.Comment;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Value
public class CommentDto {

    @NotBlank String commentId;

    @NotBlank
    String authorId;

    @NotBlank
    String authorNick;

    @NotNull
    Integer authorAge;

    @NotNull
    Integer authorGender;

    @NotBlank String hubId;

    @NotBlank String content;


    public static CommentDto of(Comment comment) {
        return new CommentDto(
                comment.getId(),
                comment.getUserId(),
                comment.getAuthorNick(),
                comment.getAuthorAge(),
                comment.getAuthorGender(),
                comment.getHubId(), comment.getContent());
    }
}
