package pt.samoisolationcore.dto;

import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
public class CreateHubRequestDto {

    @NotBlank String content;

    @NotBlank String categories;
}
