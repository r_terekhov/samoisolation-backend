package pt.samoisolationcore.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.junit4.SpringRunner;
import pt.samoisolationcore.domain.HubCategory;

import java.util.Set;

import static org.junit.Assert.assertEquals;

@DataMongoTest
@EnableMongoRepositories(basePackages = {"pt.samoisolationcore.repository"})
@RunWith(SpringRunner.class)
public class HubCategoryRepositoryTest {


    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private HubCategoryRepository hubCategoryRepository;


    @Before
    public void setUp() {
        hubCategoryRepository.deleteAll();
    }

    @Test
    public void findAllByCategoryIn() {
        HubCategory hubCategory1 = new HubCategory("playlist of day", "playlist", "some description", "emoji", 5, "https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/QLQoQm9crBM.jpg?alt=media&token=76ff2675-a94b-4a6c-a07d-0d03ac24357d");
        hubCategoryRepository.save(hubCategory1);

        HubCategory hubCategory2 = new HubCategory("games for samoisolation", "games", "some description", "emoji", 5, "https://firebasestorage.googleapis.com/v0/b/samoisolation-free.appspot.com/o/QLQoQm9crBM.jpg?alt=media&token=76ff2675-a94b-4a6c-a07d-0d03ac24357d");
        hubCategoryRepository.save(hubCategory2);

        String rawCategories = "playlist,games";
        Set<String> stringCategories = Set.of(rawCategories.split(","));

        assertEquals(2, stringCategories.size());


        Set<HubCategory> categories = hubCategoryRepository.findAllByCategoryIn(stringCategories);

        assertEquals(2, categories.size());
    }
}