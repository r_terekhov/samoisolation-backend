package pt.samoisolationcore.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.junit4.SpringRunner;


@DataMongoTest
@EnableMongoRepositories(basePackages = {"pt.samoisolationcore.repository"})
@RunWith(SpringRunner.class)
public class HubRepositoryTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private HubRepository hubRepository;

    @Autowired
    private HubCategoryRepository hubCategoryRepository;

    @org.junit.Before
    public void setUp() {
        mongoTemplate.indexOps("hubCategory").dropAllIndexes();
        mongoTemplate.indexOps("hub").dropAllIndexes();
        hubCategoryRepository.deleteAll();
        hubRepository.deleteAll();
    }

    @Test
    public void findAllByCategoriesOrderByLikesCounter() {
      /*  HubCategory hubCategory1 = hubCategoryRepository.save(new HubCategory("name1", "n1", "description", "emoji", 5, "url"));
        HubCategory hubCategory2 = hubCategoryRepository.save(new HubCategory("name2", "n2", "description", "emoji", 4, "url"));
        HubCategory hubCategory3 = hubCategoryRepository.save(new HubCategory("name3", "n3", "description", "emoji", 3, "url"));


        Hub hub1 = new Hub("author1", "body", Set.of(hubCategory1, hubCategory3));
        hub1.setLikesCounter(50L);

        Hub hub2 = new Hub("author1", "body", Set.of(hubCategory1, hubCategory3));
        hub2.setLikesCounter(10L);

        Hub hub3 = new Hub("author1", "body", Set.of(hubCategory3));
        hub3.setLikesCounter(500L);

        hubRepository.save(hub1);
        hubRepository.save(hub2);
        hubRepository.save(hub3);

        Page<Hub> hubPage = hubRepository.findAllByCategoriesInOrderByLikesCounterDesc(Set.of(hubCategory3), PageRequest.of(0, 2));
        assertEquals(3, hubPage.getTotalElements());
        assertEquals(hub3, hubPage.getContent().get(0));
        assertEquals(hub1, hubPage.getContent().get(1));*/

    }

    @Test
    public void findAllByOrderByLikesCounterDesc() {
       /* HubCategory hubCategory1 = hubCategoryRepository.save(new HubCategory("name1", "n1", "description", "emoji", 5, "url"));
        HubCategory hubCategory2 = hubCategoryRepository.save(new HubCategory("name2", "n2", "description", "emoji", 4, "url"));
        HubCategory hubCategory3 = hubCategoryRepository.save(new HubCategory("name3", "n3", "description", "emoji", 3, "url"));


        Hub hub1 = new Hub("author1", "body", Set.of(hubCategory1, hubCategory3));
        hub1.setLikesCounter(50L);

        Hub hub2 = new Hub("author1", "body", Set.of(hubCategory1, hubCategory3));
        hub2.setLikesCounter(10L);

        Hub hub3 = new Hub("author1", "body", Set.of(hubCategory3));
        hub3.setLikesCounter(500L);

        hubRepository.save(hub1);
        hubRepository.save(hub2);
        hubRepository.save(hub3);

        Page<Hub> hubPage = hubRepository.findAllByOrderByLikesCounterDesc(PageRequest.of(0, 5));
        assertEquals(3, hubPage.getTotalElements());
        assertEquals(hub3, hubPage.getContent().get(0));
        assertEquals(hub1, hubPage.getContent().get(1));
        assertEquals(hub2, hubPage.getContent().get(2));*/
    }

    @Test
    public void testIncrementDecrementLikes() {
       /* HubCategory hubCategory1 = hubCategoryRepository.save(new HubCategory("name1", "n1", "description", "emoji", 5, "url"));
        Hub hub1 = new Hub("author1", "body", Set.of(hubCategory1));
        hub1.setLikesCounter(50L);

        Hub savedHub = hubRepository.save(hub1);
        Hub incFirstTime = hubRepository.incLikesCounter(savedHub.getId());
        assertEquals(51L, incFirstTime.getLikesCounter().longValue());

        Hub incSecondTime = hubRepository.incLikesCounter(savedHub.getId());
        assertEquals(52L, incSecondTime.getLikesCounter().longValue());

        Hub decHub = hubRepository.decLikesCounter(savedHub.getId());
        assertEquals(51L, decHub.getLikesCounter().longValue());*/
    }

    @Test
    public void testIncrementDecrementComments() {
        /*HubCategory hubCategory1 = hubCategoryRepository.save(new HubCategory("name1", "n1", "description", "emoji", 5, "url"));
        Hub hub1 = new Hub("author1", "body", Set.of(hubCategory1));
        hub1.setCommentsCounter(50L);

        Hub savedHub = hubRepository.save(hub1);
        Hub incFirstTime = hubRepository.incCommentsCounter(savedHub.getId());
        assertEquals(51L, incFirstTime.getCommentsCounter().longValue());

        Hub incSecondTime = hubRepository.incCommentsCounter(savedHub.getId());
        assertEquals(52L, incSecondTime.getCommentsCounter().longValue());

        Hub decHub = hubRepository.decCommentsCounter(savedHub.getId());
        assertEquals(51L, decHub.getCommentsCounter().longValue());*/
    }

    @Test
    public void testPaginationIfPageAndSizeGreaterActual() {
       /* HubCategory hubCategory1 = hubCategoryRepository.save(new HubCategory("name1", "n1", "description", "emoji", 5, "url"));
        int pageSize = 1;
        for (int i = 0; i < 10; i++) {
            Hub hub1 = new Hub("author1" + i, "body", Set.of(hubCategory1));

            Hub savedHub = hubRepository.save(hub1);
        }

        Page<Hub> page1 = hubRepository.findAllByOrderByLikesCounterDesc(PageRequest.of(0, 20));
        assertEquals(10, page1.getTotalElements());

        Page<Hub> page2 = hubRepository.findAllByOrderByLikesCounterDesc(PageRequest.of(10, 20));
        assertEquals(10, page2.getTotalElements());*/
    }
}